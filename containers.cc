#include <cassert>
template <typename T> struct stack {
	struct node {
		T val;
		node* last;
	};
	node* top;
	stack() {
		top = nullptr;
	}
	T& operator++() {
		node* newnode = new node;
		newnode->last = top;
		top = newnode;
		return newnode->val;
	}
	T operator--() {
		T v = top->val;
		node* t = top;
		top = top->last;
		delete t;
		return v;
	}
	T& operator*() {
		return top->val;
	}
	void _delnode(node* n) {
		node* next = n->last;
		delete n;
		if (next!=nullptr) _delnode(next);
	}
	~stack() {
		if (top!=nullptr) _delnode(top);
	}
	bool empty() {return top==nullptr;}
};
template <typename T> struct list {
	struct node {
		T val;
		node* next;
	};
	node* front;
	node* back;
	size_t size;
	list() {
		front = nullptr;
		// back = nullptr;
		size = 0;
	}
	template <typename D> D& ref(D* downcast) {
		T& upcast = ++*this;
		upcast = downcast;
		return *downcast;
	}
	T& operator++() {
		node* newnode = new node;
		if (front==nullptr) {
			front=newnode;
			back=newnode;
		} else {
			back->next = newnode;
			back = newnode;
		}
		newnode->next = nullptr;
		size++;
		return newnode->val;
	}
	void operator<<(T elem) {
		auto i = ++*this;
		i=elem;
	}
	struct iterator {
		node* cur;
		void operator++()
			{ cur=cur->next; }
		bool operator!=(iterator i) const
			{ return i.cur != cur; }
		T& operator*() const
			{ return cur->val; }
	};
	iterator operator-=(iterator i) {
		if (front == i.cur) {
			node* saved=front->next;
			delete front;
			front=saved;
			iterator newi = {saved};
			return newi;
		}
		for(node* n = front;n!=nullptr;n=n->next) {
			if (n->next == i.cur) {
				node* saved;

				if (n->next!=nullptr) saved = n->next->next;
				else saved = nullptr;
				--size;
				delete n->next;
				n->next=saved;
				iterator newi = {saved};
				return newi;
			}
		}
		assert(false);
		return i;
	}
	iterator begin() const {
		iterator i = {front};
		return i;
	}
	iterator end() const {
		iterator i = {nullptr};
		return i;
	}
	void _delnode(node* n) {
		node* next = n->next;
		delete n;
		if (next != nullptr)
			_delnode(next);
	}
	~list() {
		if (front!=nullptr)
			_delnode(front);
	}
	bool empty() const { return front==nullptr; }
};