struct var {
	enum class type {flag, num} type;
	idt id;
	string label;
};
struct value {
	enum class kind { lit, tvar, ptr } kind;
	union {
		signed long lit;
		idt var;
	};
	string js() const {
		stringstream s;
		switch (kind) {
			case kind::lit: s << lit; break;
			case kind::tvar: s << "vars["<<var<<"]"; break;
			case kind::ptr: s << "vars[vars["<<var<<"]]"; break;
		}
		return s.str();
	}
};
namespace cond {
	struct cond {
		virtual string js() const = 0;
	};
	struct flag : public cond {
		idt flag;
		string js() const override {
			stringstream s;
			s << "flags["<<flag<<"] === true";
			return s.str();
		}
	};
	struct var : public cond {
		enum class comp { eq, gt, lt, gte, lte, neq } comp;
		value against;
		idt lhs;

		string js() const override {
			const static char* ops[] = {"===",">","<",">=","<=","!=="};
			stringstream s;
			s << "vars["<<lhs<<"]" << ops[(size_t)comp] << against.js();
			return s.str();
		}
	};
}