namespace node {
	struct container_node : public container, public node {
		virtual string html(const passage& p) const override {
			string h;
			for (auto& n : nodes) h += n->html(p);
			return h;
		}
		virtual string js(const passage& p) const override {
			string j;
			for (auto& n : nodes) j += n->js(p);
			return j;
		}
	};
	struct par : public container_node {
		string html(const passage& p) const override {
			string h="<p class=\"text\">";
			h+=container_node::html(p) + "</p>";
			return h;
		}
	};
	struct text : public node {
		string str;
		string html(const passage&) const override {
			return str;
		}
	};
	struct setflag : public node {
		idt flag;
		bool value;
		string js(const passage&) const override {
			stringstream s;
			s<<"flags["<<flag<<"] = "<<(value?"true":"false")<<";";
			return s.str();
		}
	};
	struct assign : public node {
		value lhs, rhs;
		string js(const passage&) const override {
			return lhs.js() + " = " + rhs.js() + ";"; // TODO add other kinds of assignment
		}
	};
	struct once : public node {
		list<node*> nodes, after;
		string html(const passage& p) const override {
			stringstream h;
			h << "<span id=\"p"<<p.id<<"-once-"<<id<<"\">";
			for (auto& n : nodes) h << n->html(p);
			h << "</span>";
			if (!after.empty()) {
				h << "<span style=\"display:none;\" id=\"p"<<p.id<<"-once-"<<id<<"-after\">";
				for (auto& n : after) h<<n->html(p);
				h << "</span>";
			}
			return h.str();
		}
		string js(const passage& p) const override {
			stringstream j;
			j << "if (in_arr("<<id<<",once)) {"
					"hide(\""<<p.id<<"-once-"<<id<<"\");";
					if (!after.empty())
						j<<"show(\""<<p.id<<"-once-"<<id<<"-after\");";
				for (auto& n : after) j<<n->js(p);
			j<<	"} else {"
					"once.push(\""<<id<<"\");"
					"show(\""<<p.id<<"-once-"<<id<<"\");";
					if (!after.empty())
						j<<"hide(\""<<p.id<<"-once-"<<id<<"-after\");";
				for (auto& n : nodes) j<<n->js(p);
			j << "}";
			return j.str();
		};
	};
	struct test : public node {
		list<node*> t;
		list<node*> f;
		cond::cond* condition;
		string html(const passage& p) const override {
			stringstream h;
			h << "<span style=\"display:none;\" id=\"p"<<p.id<<"-cond-"<<id<<"-t\">";
			for (auto& n : t) h << n->html(p);
			h << "</span>";
			if (!f.empty()) {
				h << "<span style=\"display:none;\" id=\"p"<<p.id<<"-cond-"<<id<<"-f\">";
				for (auto& n : f) h<<n->html(p);
				h << "</span>";
			}
			return h.str();
		}
		string js(const passage& p) const override {
			stringstream j;
			j << "if ("<<condition->js()<<") {"
					"show(\""<<p.id<<"-cond-"<<id<<"-t\");";
					if (!f.empty())
						j << "hide(\""<<p.id<<"-cond-"<<id<<"-f\");";
					for (auto& n : t) j<<n->js(p);
			j << " } else {"
					"hide(\""<<p.id<<"-cond-"<<id<<"-t\");";
					if (!f.empty())
						j << "show(\""<<p.id<<"-cond-"<<id<<"-f\");";
					for (auto& n : f) j<<n->js(p);
			j << "}";
			return j.str();
		};
	};
	struct gate : public container_node {
		idt to;
		idt gate;
		string html(const passage& p) const override {
			stringstream s;
			s << "<span class=\"link\" onclick=\"gate("<<id<<","<<to<<","<<gate<<")\">";
			s << container_node::html(p)+"</span>";
			return s.str();
		}
	};
	struct link : public container_node {
		idt to;
		bool push;
		bool action;
		string html(const passage& p) const override {
			stringstream s;
			s << "<span ";
			if (action) s << "id=\"pa"<<id<<"\" ";
			s << "class=\"link\" onclick=\"";
			if (action)
				s << "hide('a"<<id<<"');";
			if (push) {
				s << "gosub("<<to<<")";
			} else {
				s << "goto("<<to<<")";
			}
			s << "\">" << container_node::html(p)+"</span>";
			return s.str();
		}
	};
	struct retlink : public container_node {
		string html(const passage& p) const override {
			string s;
			s += "<span class=\"link\" onclick=\"ret()\">";
			s += container_node::html(p)+"</span>";
			return s;
		}
	};
}