#include <iostream>
#include <sstream>
#include <string.h>
#include <fstream>

using std::string;
using std::stringstream;
typedef unsigned int idt;
struct err {
	virtual void print() = 0;
};
bool verbose;
#define log(exp) if(verbose)std::cerr<<"\e[33;1mdebug:\e[0m " exp "\n"

#include "constants.cc"
#include "containers.cc"
#include "cond.cc"
#include "passage.cc"
#include "nodes.cc"
#include "story.cc"
story global_story; ///haaaaack

#include "parser.cc"
#include "compile.cc"

int main(int argc, char** argv) {
	if (argc != 2 && !(argc == 3 && strcmp(argv[2],"--verbose") == 0)) {
		std::cerr << "usage: "<<argv[0]<<" storyfile [--verbose]\n";
		return 1;
	}
	verbose = argc == 3;
	try {
		char* input;
		/* read file */ {	
			std::ifstream t;
			log("opening file "<<argv[1]<<" for reading");
			t.open(argv[1]);
			if (t.is_open()) {
				t.seekg(0,std::ios::end);
				size_t size = t.tellg();
				t.seekg(0,std::iostream::beg);

				input = new char[size+1];
				log("reading file");
				t.read(input, size);
				input[size]=0;

				log("closing file");
				t.close();
			} else {
				std::cerr << "cannot open story file for reading";
				return 2;
			}
		};
		parser::cons root;
		log("parsing input");
		parser::parse(&root, input);
		delete[] input;
		if (verbose) {
			log("successfully parsed file; parser dump");
			root.dump();
		}
		compile::compile(global_story, &root);
		std::cout<<global_story.write()<<'\n';
	} catch (err& e) {
		e.print();
		return 1;
	}

	//std::cout << j.write() << '\n';
}