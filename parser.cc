namespace parser {
	bool idcp(char c) {
		return (c >= '-' && c <= 'Z') || (c>='a' && c<='z') || c>127 || c<0; // unicode hack
	}
	bool wsp(char c) {
		return c==' ' || c=='\n' || c=='\t';
	}

	struct eparse : public err {
		const char* pos;
		const char* input;
		enum kind {bad_id_char, unexpected_eof, unrecognized_char} kind;
		eparse(const char* _pos, const char* _input, enum kind _kind) {
			pos=_pos; input=_input; kind=_kind;
		}
		const char* linestart() {
			const char* i;
			for (i = pos; *i != '\n' && i > input; --i);
			if (*i=='\n') return i+1;
			return i;
		}
		size_t linenum() {
			size_t l = 1;
			for (const char *i = input; i<pos; ++i) if (*i=='\n') ++l;
			return l;
		}
		void print() override {
			static const char* err[] = {"invalid character in id", "unexpected end of input",
			"unrecognized character"};
			std::cerr<<"\e[31;1mparse error:\e[0;1m "<<err[(size_t)kind]<<" "<<*pos<<"\e[0m\n";
			std::cerr<<"         \e[1m"<<linenum()<<" : \e[0m";
			const char* ls = linestart();
			for (const char* i = ls; *i!='\n' && *i!=0; ++i) {
				if (i==pos) std::cerr<<"\e[31;1;7m";
				std::cerr<<*i;
				if (i==pos) std::cerr<<"\e[0m";
			}
		}
	};

	struct node {
			enum kind { null = 0, atom, text, list } kind;
			void* v;
	};
	struct cons { node car; cons* cdr;
		void dump() {
			std::cerr << "(";
			if (car.kind == node::null) {
				std::cerr << ")"; return;	
			} else if (car.kind == node::list) {
				std::cerr << "[";
				((cons*)car.v)->dump();
				std::cerr << "]";
			} else if (car.kind == node::text) {
				std::cerr << "\"" << ((char*)car.v) << "\"";
			} else {
				std::cerr << (char*)car.v;;
			}
			std::cerr << " . ";
			cdr->dump();
			std::cerr << ")";
		}
	};

	void parse(cons* root, const char* input) {
		cons* cursor = root;
		string buffer = "";
		stack<cons*> pstack;
		for(const char* i = input;;++i) {
			if (*i==';') //line comment
				while (*i!='\n' && *i!=0) ++i;
			if (wsp(*i)) continue;
			if (idcp(*i)) {
				do {
					if (!idcp(*i)) throw eparse(i,input,eparse::bad_id_char);
					buffer += *i;
				} while(!wsp(i[1]) && i[1]!=']' && i[1]!=0 && i++); // this is it, the evilest thing i've ever done
				cursor->car.kind=node::atom;
				cursor->car.v=new char[buffer.size()+1];
				strcpy((char*)cursor->car.v, buffer.c_str());
				buffer="";
				cursor->cdr = new cons;
				cursor=cursor->cdr;
				continue;
			}
			if (*i==0) {
				cursor->car.kind=node::null;
				if (!pstack.empty()) {
					throw eparse(i,input,eparse::unexpected_eof);
				}
				break;
			}
			if (*i == '[') {
				++pstack = cursor;
				cursor->car.kind = node::list;
				cursor->car.v = new cons;
				cursor = (cons*)cursor->car.v;
				continue;
			}
			if (*i == '\'') {
				++i; while (*i!='\'') {
					if (*i == 0) throw eparse(i,input,eparse::unexpected_eof);
					buffer += *i++;
					if (*i == '\\' ) { buffer+=*++i; ++i; }
				}
				cursor->car.kind = node::text;
				cursor->car.v = new char[buffer.size()+1];
				strcpy((char*)cursor->car.v, buffer.c_str());
				buffer = "";
				cursor->cdr = new cons;
				cursor=cursor->cdr;
				continue;
			}
			if (*i == ']') {
				cursor->car.kind=node::null;
				cursor = --pstack;
				cursor->cdr = new cons;
				cursor = cursor->cdr;
				continue;
			}
			throw eparse(i,input,eparse::unrecognized_char);
		}
	}
}