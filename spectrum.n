'spectrum'
by 'sigfig'

start opening
no-select

colors [
	bg '#141414'
	fg '#bebebe'
	shadow 'rgb(150, 150, 150)'
	link 'rgb(255, 255, 255)'
	shadow-hover 'rgb(30, 170, 250)'
]

[passage opening
	[p 'the polycarbonate sheet that separates you from the void seems awfully thin now']
	[p 'a small emergency light weakly illuminates your controls']
	[p 'fuck, where\'s that ' [link opening/buttons 'button']]]

[passage opening/buttons
	[p 'you brush your fingers over a panel of manual controls']
	[p [link opening/power 'there it is']]]

[passage opening/power
	[p 'you toggle the aux power on']
	[p 'the generators make a faint whirring noise']
	[p 'the lights above the buttons are dancing']
	[p 'alright, ' [link opening/engine 'main engine']]]

[passage opening/engine
	[p 'the engine rumbles to life and delivers a swift kick to your lower back']
	[p 'you\'re moving forwards now, but the view outside doesn\'t believe it']
	[p 'the ' [link computer/boot 'computers'] ' are booting up']]

[passage computer/boot crt-boot
	[p [link computer/main 'ALTUS CONTROL SYSTEMS']]]

[passage computer/main crt
	[p 'todo']]

[style opening-style
	[background 'rgb(20, 20, 20)']
	[.text
		[font-family 'sans-serif']
		[color 'rgb(190, 190, 190)']
		[text-shadow '0px 0px 0.5em rgb(150, 150, 150)']]
	[.link
		[font-family 'sans-serif']
		[color 'rgb(255, 255, 255)']]
	[.link:hover
		[text-shadow '0px 0px 1em rgb(30, 170, 250)']]]

[style crt-boot
	[background-color '#8cc2ff']
	[.link
		[display 'block']
		[text-align 'center']
		[font-family 'sans-serif']
		[color '#fff']
		[font-size '72pt']
		[text-shadow '0px 0px 2em #fff']]
	[.link:hover
		[text-shadow '0px 0px 2em #fff']]]

[style crt
	[background-color 'rgb(0, 0, 0)']
	[.link
		[display 'block']]
]