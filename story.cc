struct story {
	list<char*> flags;
	list<char*> vars;
	list<passage> passages;
	idt start;
	bool noselect;
	bool nostyle;
	const char* style;
	struct {
		string bg, fg, shadow, link, hover, shadowhover;
	} colors;
	story() {
		nostyle = noselect = false;
		style = nullptr;
		// default stylesheet
		colors.bg = "black";
		colors.fg = "#f0f0f0";
		colors.shadow = "#adadad";
		colors.link = "white";
		colors.hover = "#f0ffff";
		colors.shadowhover = "#addede";
	}
	char* title, *author;
	string html() {
		stringstream h;
		idt i = 0; //haaack
		for (passage& p : passages) {
			h << p.html(i==start);
			++i;
		}
		return h.str();
	}
	string js() {
		stringstream j;
		for (passage& p : passages) {
			string jsc = p.js();
			if (p.style != nullptr) {
				j << "styled["<<p.id<<"] = \""<<p.style<<"\";";
			}
			if (jsc != "") {
				j << "onEnter[" << p.id << "] = function(){";
				j << jsc;
				j << "};";
			}
		}
		return j.str();
	}
	string wscript() {
		stringstream j;
		if (!flags.empty()) {
			j << "var flags=[];"
			"for (var i=0;i<"<<flags.size<<";++i)flags[i]=false;";
		}
		if (!vars.empty()) {
			j << "var vars=[];"
			"for (var i=0;i<"<<vars.size<<";++i)vars[i]=0;";
		}
		j << "window.onload=function(){pos="<<start<<";goto("<<start<<")};"; //haaaaaaaaaack
		/* runtime */
		// this is an abjectly shitty way of doing this; in the long term i'd
		// like to add an option that allows storing the runtime separately
		// (i.e. so libraries of games don't duplicate the runtime over & over)
		// but for now this is the best way to do it.
		j << constants::runtime;
		/* end runtime */
		j << js();
		return j.str();
	};

	string usercss;
	string wstyle() {
		string css;
		if (!nostyle) {
			css+="body {"
		    	"cursor: default;"
		   		"background: "+colors.bg+";"
			"}"
			"::selection {"
			    "background-color: rgba(217, 165, 189, 0.17);"
			"}"
			".passage {"
			    "max-width: 40em;"
			    "margin: auto;"
			    "margin-top: 5em;"
			"}"
			".text {"
			    "color: "+colors.fg+";"
			    "text-shadow: 0px 0px 1em "+colors.shadow+";"
			    "font-size: 18pt;"
			    "font-family: GaramondNo8, serif;"
			    "font-weight: 100;"
			"}"
			".link {"
			    "color: "+colors.link+";"
			    "font-weight: bold;"
			    "cursor: pointer;"
			    "transition: .2s;"
			"}"
			".link:hover {"
			    "transition: 0s;"
			    "color: "+colors.hover+";"
			    "text-shadow: 0px 0px 1em "+colors.shadowhover+";"
			"}";
		}
		if (noselect) {
			css+="* { -webkit-touch-callout: none;-webkit-user-select: none;"
			"-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;"
			"user-select: none; } ";
		}
		css+=usercss;
		return css;
	}
	string write() {
		string h;
		try {
			h += std::string("<html><head><meta charset=\"UTF-8\"><style type=\"text/css\">") + wstyle() + "</style>"
			"<script type=\"text/javascript\">" + wscript() + "</script>"
			+ "<title>" + title + "</title></head><body";
			if (style != nullptr)
				h+=string(" class=\"")+style+"\"";
			h += ">" + html() + "</body></html>";
		} catch (const char* n) {
			exit(1);
		}
		return h;
	}
};
