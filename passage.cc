struct passage;
namespace node {
	struct node {
		virtual string html(const passage&) const {return "";}
		virtual string js(const passage&) const {return "";}
		static size_t makeid() {
			static size_t init = 0;
			return init++;
		}
		size_t id = makeid();
	};
	struct container {
		list<node*> nodes;
	};
}
struct passage : public node::container {
	idt id;
	const char* style;
	string html(bool visible) const {
		stringstream html;
		html << "<div class=\"passage\"";
		if(!visible) html << "style=\"display:none;\" ";
		html << "id=\"p" << id << "\">";
		for (auto& n : nodes) {
			html << n->html(*this);
		}
		html << "</div>";
		return html.str();
	}
	string js() const {
		string j = "";
		for(auto& n : nodes) {
			j += n->js(*this);
		}
		return j;
	}
};