namespace compile {
	struct ecompile : public err {
		void print() override {
			std::cerr<<"\e[31;1mcompile error:\e[0m ";
		};
	};
	struct etype : public ecompile {
		enum parser::node::kind expected, got;
		const char* ref;
		etype(enum parser::node::kind e, enum parser::node::kind g, const char* r) {
			expected = e; got = g; ref = r;
		}
		void print() override {
			ecompile::print();
			const static char* types[] = {"null","atom","text","list"};
			std::cerr<<"invalid type: expected \e[1m"<<types[(size_t)expected]<<"\e[0m, got \e[1m"<<types[(size_t)got]<<"\e[0m for "<<ref<<"\n";
		}
	};
	struct einvalid : public ecompile {
		const char* thing, *lit;
		einvalid(const char* t, const char* l) { thing = t; lit = l; }
		void print() override {
			ecompile::print();
			std::cerr<<"invalid "<<thing<<" \""<<lit<<"\"\n";
		}
	};
	struct emissing : public ecompile {
		const char* thing;
		emissing(const char* t) { thing = t; }
		void print() override {
			ecompile::print();
			std::cerr<<"missing \e[1m"<<thing<<"\e[0m\n";
		}
	};
	struct eunrec : public ecompile {
		const char* term;
		eunrec(const char* t) { term = t; }
		void print() override {
			ecompile::print();
			std::cerr<<"unrecognized keyword \""<<term<<"\"\n";
		}
	};
	char* astext(const parser::cons* c, const char* ref = "") {
		if (c->car.kind == parser::node::text) {
			return (char*)c->car.v;
		} else {
			throw etype(parser::node::text,c->car.kind, ref);
		}
	}
	char* asatom(const parser::cons* c, const char* ref = "") {
		if (c->car.kind == parser::node::atom) {
			return (char*)c->car.v;
		} else {
			throw etype(parser::node::atom, c->car.kind, ref);
		}
	}
	parser::cons* aslist(const parser::cons* c, const char* ref = "") {
		if (c->car.kind == parser::node::list) {
			return (parser::cons*)c->car.v;
		} else {
			throw etype(parser::node::list,c->car.kind, ref);
		}
	}
	bool eq(const parser::cons* c, const char* test) {
		return strcmp((char*)c->car.v, test) == 0;
	}
	bool atomeq(const parser::cons* c, const char* test, const char* ref) {
		char* t = asatom(c, ref);
		return strcmp(t,test)==0;
	}
	typedef list<node::node*>* nodelist;

	idt find_id(list<char*>& list, const char* title) {
		idt i = 0;
		for(auto& p : list) {
			if (strcmp(p,title)==0) return i;
			++i;
		}; //else
		throw einvalid("ID", title);
	}

	void pnode(nodelist n, parser::cons* data);
	void linknode(nodelist n, parser::cons* data, bool push);
	void retnode(nodelist n, parser::cons* data);
	void setnode(nodelist n, parser::cons* data, bool value);
	void flagp(nodelist n, parser::cons* data);
	void oncep(nodelist n, parser::cons* data);
	void container_dispatch(nodelist n, parser::cons* data) {
		for(parser::cons* c = data; c->car.kind != parser::node::null; c=c->cdr) {
			if (c->car.kind == parser::node::text) { //special syntax for text node
				auto& t = n->ref(new node::text);
				t.str = (char*)c->car.v;
			} else if (c->car.kind == parser::node::list) {
				parser::cons* node = aslist(c, "node");
				const char* kind = asatom(node, "node type");
				if (strcmp(kind,"p")==0)			pnode		(n, node->cdr);
				else if (strcmp(kind,"link")==0)	linknode	(n, node->cdr,false);
				else if (strcmp(kind,"sub")==0)		linknode	(n, node->cdr,true);
				else if (strcmp(kind,"ret")==0)		retnode		(n, node->cdr);
				else if (strcmp(kind,"flag?")==0)	flagp		(n, node->cdr);
				else if (strcmp(kind,"once")==0)	oncep		(n, node->cdr);
				else if (strcmp(kind,"set")==0)		setnode		(n, node->cdr, true);
				else if (strcmp(kind,"clear")==0)	setnode		(n, node->cdr, false);
				else {
					throw einvalid("node type",(char*)node->car.v);
				}
			} else {
				throw etype(parser::node::list,c->car.kind,"passage nodes");
			}
		}
	}
	void pnode(nodelist n, parser::cons* data) {
		auto& p = n->ref(new node::par);
		container_dispatch(&p.nodes, data);
	}
	void setnode(nodelist n, parser::cons* data, bool value) {
		auto& p = n->ref(new node::setflag);
		p.flag = find_id(global_story.flags, asatom(data,"flag ID"));
		p.value = value;
		if (data->cdr->car.kind != parser::node::null)
			throw etype(parser::node::null, data->cdr->car.kind, "set nodelist");
	}
	void flagp(nodelist n, parser::cons* data) {
		auto& c = n->ref(new node::test);
		cond::flag* cf = new cond::flag;
		c.condition=cf;
		cf -> flag=find_id(global_story.flags, asatom(data,"flag ID"));
		data=data->cdr;
		container_dispatch(&c.t, aslist(data,"list for #t"));
		data=data->cdr;
		if (data->car.kind != parser::node::null)
			container_dispatch(&c.f, aslist(data,"list for #f"));
	}
	void oncep(nodelist n, parser::cons* data) {
		auto& c = n->ref(new node::once);
		container_dispatch(&c.nodes, aslist(data,"list for #t in once clause"));
		data=data->cdr;
		if (data->car.kind != parser::node::null)
			container_dispatch(&c.after, aslist(data,"list for #f in once clause"));
	}
	struct pref { const char* title; const char* style; parser::cons* data; };
	list<pref> prefs;
	idt find_pref(const char* title) {
		idt i = 0;
		for(auto& p : prefs) {
			if (strcmp(p.title,title)==0) return i;
			++i;
		}; //else
		throw einvalid("passage ID", title);
	}

	// im bad
	struct style { const char* name; parser::cons* data; };
	list<style> styles;
	idt find_style(const char* name) {
		idt i = 0;
		for(auto& s : styles) {
			if(strcmp(s.name, name) == 0) return i;
			++i;
		};
		throw einvalid("style ID", name);
	}

	void linknode(nodelist n, parser::cons* data, bool push) {
		auto& l = n->ref(new node::link);
		l.push=push;
		l.to=find_pref(asatom(data,"page ID"));
		container_dispatch(&l.nodes, data->cdr);
	}
	void retnode(nodelist n, parser::cons* data) {
		auto& l = n->ref(new node::retlink);
		container_dispatch(&l.nodes, data);
	}

	// naively outputs css, definitely want to do more processing here in the future
	// effectful functions are alien and new what happened to me i used to be normal
	string gen_css(style s) {

		string css;
		string global; // ugh
		const char* style_name = s.name;

		//how do i fold in c????
		//its for right? you can iterate with for?
		for(parser::cons* cur = s.data; cur->car.kind != parser::node::null; cur = cur->cdr) {
			parser::cons* slot = (parser::cons*)cur->car.v;
			
			if (slot->cdr->car.kind == parser::node::list) { //global
				const char* slot_name = asatom(slot, "style slot identifier");
				css += string(".") + style_name + " " + slot_name + "{"; //"\n";
				
				for(parser::cons* l = slot->cdr; l->car.kind != parser::node::null; l = l->cdr) {
					parser::cons* elem = (parser::cons*)l->car.v;
					const char* rule = asatom(elem, "css rule");
					const char* expr = astext(elem->cdr, "css expression");
					css += /* "\t" + */ string(rule) + ":" + expr + ";"; // "\n";
				}

				css += "}";
			} else {
				// this is so cheating
				const char* rule = asatom(slot, "css rule");
				const char* expr = astext(slot->cdr, "css expression");
				global += /* "\t" + */ string(rule) + ":" + expr + ";"; // "\n";
			}
		}
		if (!global.empty()) { //ewwww
			css += string(".") + style_name + "{"; //"\n";
			css += global;
			css += "}";
		}
		return css;
	}

	void compile(story& st, parser::cons* root) {
		st.title = astext(root, "title"); //title is always first element
		char* start_at = nullptr;
		for (parser::cons* cur = root->cdr;cur->car.kind!=parser::node::null;cur=cur->cdr) {
			switch (cur->car.kind) {
				case parser::node::atom:
					if (eq(cur,"by")) {
						cur=cur->cdr;
						st.author = astext(cur, "author");
						continue;
					} else if (eq(cur,"default-style")) {
						cur=cur->cdr;
						st.style = asatom(cur, "style");
						continue;
					} else if (eq(cur,"start")) {
						cur=cur->cdr;
						start_at = asatom(cur, "starting passage");
						continue;
					} else if (eq(cur,"flags")) {
						cur=cur->cdr;
						for (parser::cons* lflags = aslist(cur,"flag list");
						lflags->car.kind != parser::node::null; lflags = lflags->cdr) {
							char*& f = ++st.flags;
							f = asatom(lflags, "flag ID");
						}
						continue;
					} else if (eq(cur,"vars")) {
						cur=cur->cdr;
						for (parser::cons* lvars = aslist(cur,"variable list");
						lvars->car.kind != parser::node::null; lvars = lvars->cdr) {
							char*& v = ++st.vars;
							v = asatom(lvars, "variable ID");
						}
						continue;
					} else if (eq(cur,"no-select")) {
						st.noselect=true;
						continue;
					} else if (eq(cur,"colors")) {
						cur=cur->cdr;
						for (parser::cons* lsty = aslist(cur,"default stylesheet");
						lsty->car.kind != parser::node::null; lsty = lsty->cdr) {
							const char* f = asatom(lsty, "element");
							lsty = lsty->cdr;
							if (lsty->car.kind == parser::node::null) {
								throw einvalid("color spec", "(null)");
							}
							if (strcmp(f, "bg")==0) {
								st.colors.bg = astext(lsty, "background color spec");
							} else if (strcmp(f, "fg")==0) {
								st.colors.fg = astext(lsty, "foreground color spec");
							} else if (strcmp(f, "shadow")==0) {
								st.colors.shadow = astext(lsty, "shadow color spec");
							} else if (strcmp(f, "link")==0) {
								st.colors.link = astext(lsty, "link color spec");
							} else if (strcmp(f, "hover")==0) {
								st.colors.hover = astext(lsty, "hover color spec");
							} else if (strcmp(f, "shadow-hover")==0) {
								st.colors.shadowhover = astext(lsty, "shadow hover color spec");
							} else {
								throw einvalid("element", f);
							}
						}
						continue;
					}
					throw eunrec((char*)cur->car.v);
				break;
				case parser::node::list:{
					parser::cons* l = (parser::cons*)cur->car.v;
					if(atomeq(l,"passage","list tag")) {
						l=(parser::cons*)l->cdr;
						pref& p = ++prefs;
						p.title=asatom(l,"passage title");
						l=(parser::cons*)l->cdr; //traverse one more, probably a better way to do this?
						/////
						if (l->car.kind == parser::node::atom) {
							p.style=asatom(l,"passage style");
							p.data=l->cdr;
						} else {
							p.style=nullptr;
							p.data=l;
						}
						continue;
					} else if(atomeq(l,"style","list tag")) {
						l=(parser::cons*)l->cdr;
						style& s = ++styles;
						s.name=asatom(l,"style name");
						s.data=l->cdr;
					} else {
						throw einvalid("list tag",(char*)l->car.v);
					}}
				break;
				default:;
			}
		}
		idt i=0;
		if (start_at == nullptr) throw emissing("start passage");
		st.start = find_pref(start_at);

		for (style& s : styles) {
			st.usercss += gen_css(s);
		}

		for (pref& pr : prefs) {
			passage& p = ++st.passages;
			p.id=i;
			p.style=pr.style;
			container_dispatch(&p.nodes, pr.data);
			++i;
		}
	}
}