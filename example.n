'Bone Heresy'
by 'Summer Hale'

start it-is-dark
no-select

colors [
	bg '#29000e'
	fg '#ff8ab3'
	shadow '#eb0052'
	link '#ffc2d7'
	hover '#fff0f5'
	shadow-hover '#ff6198'
]

[style advanced-styling
	[background '#29000e']
	[transition '2s']
]

default-style advanced-styling

flags [
	hungry
	thirsty
	bloodsoaked
	dressed
]

[style memory
	[background 'black']
	[transition '5s']]

[style dark-memory
	[background '#100000']
	[transition '1s']
	[.text
		[color '#ff3030']
		[transition '1s']]
	]

[passage it-is-dark
	[set hungry]
	[set thirsty]
	[p {You awake, your head pounding.}]
	[p "It is dark."]
	[p 'You remember nothing.']
	[p [link corridor/listen 'Listen'] ' to your surroundings']
]

[passage you
	[p [flag? dressed [
		'You are dressed in dark robes.'
	][
		'You are wearing a tank top and a miniskirt.'
	]]]

	[p [flag? bloodsoaked
		['You are spattered with blood.']
	]]

	[ret 'Back']
]

[passage corridor
	[p 'It is still dark.']
	[p 'You '[link remember/1 'remember'] ' very little.']
	[p [sub you 'You'] ' are very cold.']
	[p [link corridor/listen 'Listen'] ' to your surroundings']
]
[passage corridor/listen
	[flag? bloodsoaked [
		[p 'Blood drips from pipes far overhead into a bucket.']
	][
		[p 'You hear a fluid dripping nearby.']
		[p 'It splashes like it\'s falling into some sort of ' [link bucket 'bucket'] ' or tank.']
	]]
	[p 'You realize you are incredibly hungry and thirsty.']
	[p [link pocket 'Check your pockets'] '.']
]

[passage bucket
	[p 'You grope for a bucket. Maybe there\'s water&mdash;']
	[p 'Your shaking hand finds the bucket and it spills violently. You feel a thick liquid splash against your skin. You smell&mdash;']
	[p 'Blood.']
	[p 'Your clothes and skin and splattered with blood.']
	[set bloodsoaked]
	[p 'You ' [link corridor/listen 'stumble backwards']]
]

[passage pocket
	[p 'You fumble for your pockets. You prod at unyielding fabric for a moment before realizing you\'re still wearing your miniskirt with the fake pocke&mdash;']
	[p '&mdash;still&mdash;']
	[p 'You begin to ' [link remember/1 'remember'] '.']
]

[passage remember/1
	[p 'It\'s difficult, like the memories don\'t want to come back. When they do, they come in flashes. They don\'t feel like yours.']

	[p [link remember/party 'A party.']]

	[p [link remember/sky 'A jet-black sky.']]

	[p [link remember/snarling 'Snarling.']]

	[p 'Focus on '[link corridor 'what\'s around you']]
]

[passage remember/snarling memory
	[p 'It was a police raid.']
	[p 'A press of bodies, huddled against the wall. A roar as the packs filled the street like an endless tide of teeth and claws.']
	[p 'The relief.']
	[p [link remember/snarling/not-you 'They didn\'t want you'] '.']
]

[passage remember/snarling/not-you memory
	[p 'A few unlucky people who had stepped out for a smoke were taken. A few neighbors, out staring at the sky, ignoring the warnings bleating out from the radio. Some children, out past curfew. Their blood streaked the streets.']
	[p 'The police are never clean, but they\'ve been especially vicious since the mating season started.']
	[p 'But they weren\'t here for ' [link remember/party 'the party']'. They were after ' [link remember/snarling/someone-else 'someone else'] '.']
]

[passage remember/snarling/someone-else memory
	[p 'Who?']
	[p 'Nothing.']
	[p 'You know you knew once. Your memories point at it, the links are all still there, but the thing they\'re pointing at isn\'t. Like there\'s a hole in the middle of your memory, a hole as dark and wrong as the ' [link remember/sky 'jet-black sky'] '.']
	[p 'It feels like something was ripped out of your head.']
	[p 'Messily.']
	[p 'Your heart begins racing.']
	[p [link remember/1 'You don\'t want to think about this anymore.']]
]
[passage remember/sky memory
	[p '"Do not look at the sky."']
	[p 'Somewhere a radio was tuned to Approved News 6. For two hours it broadcast the same advisory. "Do not look at the sky." The ever-shifting voice of the announcer had taken on unusual tones of urgency. "Loyal citizens are urgently advised to <em>avoid observing the sky</em>."']
	[p [link remember/sky/look 'You looked.']]
]
[passage remember/sky/look memory
	[p 'There were no stars. There was no moon. There was only emptiness&mdash;']
	[p 'No.']
	[p 'There was something else.']
	[p 'You can hear your heart thumping in your chest.']
	[p [link remember/1 'You don\'t want to keep thinking about this.']]
]
[passage remember/party memory
	[p 'People shouting, laughing, dancing, kissing. A cute girl you laid your eyes on longingly, ' [link remember/cute-girl 'but turned away because...']]
	[p 'Nothing.']
	[p 'There was music.']
	[p [link remember/house 'It was someone\'s house.']]
]
[passage remember/house memory
	[p 'It was small. The walls were tan.']
	[p 'Could have been any house.']
	[p 'Except for that ' [link remember/thing '<em>thing</em>'] '.']
]
[passage remember/thing dark-memory
	[p 'The thought is physically painful. You struggle not to vomit.']
	[p 'There wasn\'t anything right about '[link remember/party 'that party'] ', not with that <em>thing</em> and ' [link remember/sky 'that sky'] '.']
]
[passage remember/cute-girl memory
	[p 'Who was she?']
	[p 'You remember her face, her body. Freckled, slim, sexy, <em>ruthless&mdash;</em>']
	[p 'The way she danced was almost inhuman.']
	[p '<em>she knew she knew '[link remember/cute-girl/nothing 'SHE KNEW'] '</em>']
]
[passage remember/cute-girl/nothing
	[p '...nothing.']
	[p [link remember/1 'The spark of memory fades quicker than it flared.']]
]
